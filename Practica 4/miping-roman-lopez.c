//Práctica tema 8, Román López Antonio
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include "ip-icmp-ping.h"

unsigned short calcChecksum(void *msg, int tam)
{
    int numShorts = tam / 2;
    unsigned short int *pointer;
    unsigned int acum = 0;
    pointer = msg;

    int i;
    for (i = 0; i < numShorts; i++)
    {
        acum = acum + (unsigned int)*pointer;
        pointer++;
    }
    acum = (acum >> 16) + (acum & 0x0000ffff);
    acum = (acum >> 16) + (acum & 0x0000ffff);
    acum = ~acum;

    return acum;
}

int main(int argc, char *argv[])
{
    if (argc > 3)
    {
        fprintf(stderr, "%s Too much arguments\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    //Create the struct for the icmp request
    ECHORequest icmp;
    memset(&icmp, 0, sizeof(icmp));
    if (argc > 2 && !strcmp("-v", argv[2]))
        {
            printf("-> Generando cabecera ICPM\n");
        }
    icmp.icmpHeader.Type = 8;
    icmp.ID = getpid();
    strcpy(icmp.payload, "Hello");
    icmp.icmpHeader.Checksum = calcChecksum(&icmp, sizeof(icmp));
    //Option for -v argument
    if (argc > 2 && !strcmp("-v", argv[2]))
        {
            printf("-> Type: %i.\n", icmp.icmpHeader.Type);
            printf("-> Code: %i.\n", icmp.icmpHeader.Code);
            printf("-> Identifier(pid): %i.\n", icmp.ID);
            printf("-> Seq.Number: %i.\n", icmp.SeqNumber);
            printf("-> Cadena a enviar: %s.\n", icmp.payload);
            printf("-> Checksum: %x.\n", icmp.icmpHeader.Checksum);
            printf("-> Tamano total del paquete ICMP: %lu.\n", sizeof(icmp));
        }

    //Struct in_addr type to save the IP passed at the parameter.
    struct in_addr addr;
    int err = inet_aton(argv[1], &addr);
    if (err == 0)
    {
        // Print error code and exit.
        perror("inet_atom()");
        exit(EXIT_FAILURE);
    }

    //Create sockaddr_in structs for local and server addresses
    struct sockaddr_in remoteaddr, localaddr;
    remoteaddr.sin_addr.s_addr = addr.s_addr;
    remoteaddr.sin_family = AF_INET;
    localaddr.sin_family = AF_INET;
    localaddr.sin_addr.s_addr = INADDR_ANY;
    socklen_t remoteaddr_len = sizeof(struct sockaddr);

    //Create the socket for the conexion
    int sock = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
    if (sock < 0)
    {
        // Print error code and exit.
        perror("socket()");
        exit(EXIT_FAILURE);
    }

    //Bind the socket to local address
    if (bind(sock, (struct sockaddr *)&localaddr, sizeof(localaddr)) < 0)
    {
        // Print error code and exit.
        perror("bind()");
        exit(EXIT_FAILURE);
    }

    //Send ping
    if (sendto(sock, &icmp, sizeof(icmp), 0, (struct sockaddr *)&remoteaddr, sizeof(remoteaddr)) < 0)
    {
        // Print error code and exit.
        perror("sendto()");
        exit(EXIT_FAILURE);
    }
    printf("Paquete ICMP enviado a %s\n", inet_ntoa(remoteaddr.sin_addr));

    ECHOResponse answ;
    memset(&answ, 0, sizeof(answ));

    //Receive the response
    if (recvfrom(sock, &answ, sizeof(answ), 0, (struct sockaddr *)&remoteaddr, &remoteaddr_len) < 0)
    {
        // Print error code and exit.
        perror("recvfrom()");
        exit(EXIT_FAILURE);
    }
    printf("Respuesta recibida desde %s\n", inet_ntoa(remoteaddr.sin_addr));

    //Option for -v argument
    if (argc > 2 && !strcmp("-v", argv[2]))
        {
            printf("-> Tamano de la respuesta: %lu.\n", sizeof(answ));
            printf("-> Cadena recibida: %s.\n", answ.payload);
            printf("-> Identifier(pid): %i.\n", answ.ID);
            printf("-> TTL: %i.\n", answ.ipHeader.TTL);
        }
    //Switch to select the response with type and code
    char msg[512];
    switch (answ.icmpHeader.Type)
    {
    case 0:
        strcpy(msg, "Respuesta correcta");
        break;
    case 3:
        strcpy(msg, "Destination unreachable: ");
        switch (answ.icmpHeader.Code)
        {
        case 0:
            strcat(msg, "Destination network unreachable");
            break;
        case 1:
            strcat(msg, "Destination host unreachable");
            break;
        case 2:
            strcat(msg, "Destination protocol unreachable");
            break;
        case 3:
            strcat(msg, "Destination port unreachable");
            break;
        case 4:
            strcat(msg, "Fragmentation required, and DF flag set");
            break;
        case 5:
            strcat(msg, "Source route failed");
            break;
        case 6:
            strcat(msg, "Destination network unknown");
            break;
        case 7:
            strcat(msg, "Destination host unknown");
            break;
        case 8:
            strcat(msg, "Source host isolated");
            break;
        case 9:
            strcat(msg, "Network administratively prohibited");
            break;
        case 10:
            strcat(msg, "Host administratively prohibited");
            break;
        case 11:
            strcat(msg, "Network unreachable for ToS");
            break;
        case 12:
            strcat(msg, "Host unreachable for ToS");
            break;
        case 13:
            strcat(msg, "Communication administratively prohibited");
            break;
        case 14:
            strcat(msg, "Host Precedence Violation");
            break;
        case 15:
            strcat(msg, "Precedence cutoff in effect");
            break;
        }
        break;
    case 5:
        strcpy(msg, "Redirect message: ");
        switch (answ.icmpHeader.Code)
        {
        case 0:
            strcat(msg, "Redirect Datagram for the Network");
            break;
        case 1:
            strcat(msg, "Redirect Datagram for the Host");
            break;
        case 2:
            strcat(msg, "Redirect Datagram for the ToS & network");
            break;
        case 3:
            strcat(msg, "Redirect Datagram for the ToS & host");
            break;
        }
        break;
    case 9:
        strcpy(msg, "Router Advertisement");
        break;
    case 10:
        strcpy(msg, "Router discovery/selection/solicitation");
        break;
    case 11:
        strcpy(msg, "Time Exceeded: ");
        switch (answ.icmpHeader.Code)
        {
        case 0:
            strcat(msg, "TTL expired in transit");
            break;
        case 1:
            strcat(msg, "Fragment reassembly time exceeded");
            break;
        }
        break;
    }

    printf("Descripcion de la respuesta: %s(Type: %d, Code: %d).\n", msg, answ.icmpHeader.Type, answ.icmpHeader.Code);

    //Close the socket used
    if (close(sock) < 0)
    {
        // Print error code and exit.
        perror("close()");
        exit(EXIT_FAILURE);
    }

    return 0;
}
