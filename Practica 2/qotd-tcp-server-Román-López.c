// Practica tema 6, Román López Antonio
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char *argv[])
{

    if (argc > 3)
    {
        fprintf(stderr, "%s Too much arguments\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    int port;
    if (argc == 3 && strcmp(argv[0], "-p"))
    {
        port = htons(atoi(argv[2]));
    }
    else
    {
        struct servent *serv = getservbyname("qotd", "tcp");
        port = serv->s_port;
    }
    char buffQuote[512];
    struct sockaddr_in servaddr, clientaddr;
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = INADDR_ANY;
    servaddr.sin_port = port;
    socklen_t clientaddr_len = sizeof(struct sockaddr);

    //Create the socket for the conexion
    int sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0)
    {
        // Print error code and exit.
        perror("socket()");
        exit(EXIT_FAILURE);
    }
    //Bind the socket to local address
    if (bind(sock, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)
    {
        // Print error code and exit.
        perror("bind()");
        exit(EXIT_FAILURE);
    }

    //Define the max number of pending requests
    if (listen(sock, 10) < 0)
    {
        // Print error code and exit.
        perror("listen()");
        exit(EXIT_FAILURE);
    }
    while (1)
    {
        //Accept the request and generate the conexion socket
        int conSock = accept(sock, (struct sockaddr *)&clientaddr, &clientaddr_len);
        if (conSock < 0)
        {
            // Print error code and exit.
            perror("accept()");
            exit(EXIT_FAILURE);
        }
        //Create the child for dispatch the request
        int id = fork();
        if (id < 0)
        {
            // Print error code and exit.
            perror("fork()");
            exit(EXIT_FAILURE);
        }
        if (id == 0)
        {
            memset(buffQuote, 0, sizeof(buffQuote));
            //Create the file with the quote from fortune
            system("/usr/games/fortune -s > /tmp/tt.txt");
            FILE *fich = fopen("/tmp/tt.txt", "r");
            if (fich == NULL)
            {
                // Print error code and exit.
                perror("fopen()");
                exit(EXIT_FAILURE);
            }
            int nc = 0;
            char aux;
            //Read the file until EOF
            while (1)
            {
                aux = fgetc(fich);
                if (aux != EOF)
                    buffQuote[nc] = aux;
                else
                    break;
                nc++;
            }

            //Close the file and clean the buffer for next request
            if (fclose(fich))
            {
                // Print error code and exit.
                perror("fclose()");
                exit(EXIT_FAILURE);
            }
            char intro[512] = "Quote of the day from vm2502:\n";
            strcat(intro, buffQuote);
            memset(buffQuote, 0, sizeof(buffQuote));

            //Send a String to the server tohave an answer
            if (send(conSock, intro, 512, 0) < 0)
            {
                // Print error code and exit.
                perror("sendto()");
                exit(EXIT_FAILURE);
            }

            //Close the fork socket used
            if (close(conSock) < 0)
            {
                // Print error code and exit.
                perror("close()");
                exit(EXIT_FAILURE);
            }
        }
    }
    //Notify end of connection
    if (shutdown(sock, SHUT_RDWR))
    {
        // Print error code and exit.
        perror("shutdown()");
        exit(EXIT_FAILURE);
    }

    //Close the father socket used
    if (close(sock) < 0)
    {
        // Print error code and exit.
        perror("close()");
        exit(EXIT_FAILURE);
    }

    return 0;
}