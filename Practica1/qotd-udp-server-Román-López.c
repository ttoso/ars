// Practica tema 5, Román López Antonio
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char *argv[])
{

    if (argc > 3)
    {
        fprintf(stderr, "%s Too much arguments\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    int port;
    if (argc == 2 && strcmp(argv[0], "-p"))
    {
        port = htons(atoi(argv[2]));
    }
    else
    {
        struct servent *serv = getservbyname("qotd", "udp");
        port = serv->s_port;
    }
    char dtg[512];
    static char buffQuote[512];
    struct sockaddr_in servaddr, clientaddr;
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = INADDR_ANY;
    servaddr.sin_port = port;
    socklen_t clientaddr_len = sizeof(struct sockaddr);

    //Create the socket for the conexion
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0)
    {
        // Print error code and exit.
        perror("socket()");
        exit(EXIT_FAILURE);
    }
    //Bind the socket to local address
    if (bind(sock, (struct sockaddr *)&servaddr, sizeof(servaddr)) < 0)
    {
        // Print error code and exit.
        perror("bind()");
        exit(EXIT_FAILURE);
    }
    while (1)
    {
        //Revceive the quote from the server
        if (recvfrom(sock, dtg, 512, 0, (struct sockaddr *)&clientaddr, &clientaddr_len) < 0)
        {
            // Print error code and exit.
            perror("recvfrom()");
            exit(EXIT_FAILURE);
        }

        //Create the file with the quote from fortune
        system("/usr/games/fortune -s > /tmp/tt.txt");
        FILE *fich = fopen("/tmp/tt.txt", "r");
        if (fich == NULL)
        {
            // Print error code and exit.
            perror("fopen()");
            exit(EXIT_FAILURE);
        }
        int nc = 0;
        char aux;
        //Read the file until EOF
        while (1)
        {
            aux = fgetc(fich);
            if (aux != EOF)
                buffQuote[nc] = aux;
            else
                break;
            nc++;
        }

        //Close the file and clean the buffer for next request
        if (fclose(fich))
        {
            // Print error code and exit.
            perror("fclose()");
            exit(EXIT_FAILURE);
        }
        char intro[512] = "Quote of the day from vm2502:\n";
        strcat(intro, buffQuote);
        memset(buffQuote, 0, sizeof(buffQuote));

        //Send a String to the server tohave an answer
        if (sendto(sock, intro, strlen(intro), 0, (struct sockaddr *)&clientaddr, sizeof(clientaddr)) < 0)
        {
            // Print error code and exit.
            perror("sendto()");
            exit(EXIT_FAILURE);
        }
    }

    //Close the socket used
    if (close(sock) < 0)
    {
        // Print error code and exit.
        perror("close()");
        exit(EXIT_FAILURE);
    }

    return 0;
}