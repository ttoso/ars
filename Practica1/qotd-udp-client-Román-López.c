// Practica tema 5, Román López Antonio
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char *argv[])
{
    if (argc > 4)
    {
        fprintf(stderr, "%s Too much arguments\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    //Struct in_addr type to save the IP passed at the parameter.
    struct in_addr addr;
    int err = inet_aton(argv[1], &addr);
    if (err == 0)
    {
        // Print error code and exit.
        perror("inet_atom()");
        exit(EXIT_FAILURE);
    }

    //Asign port number given from the user or use the asigned for the service

    int port;
    if (argc >= 3 && strcmp(argv[1], "-p"))
    {
        port = htons(atoi(argv[3]));
    }
    else
    {
        struct servent *serv = getservbyname("qotd", "udp");
        port = serv->s_port;
    }

    //Create the Strings
    char answ[512];
    char ask[512] = "Hello";
    //Create sockaddr_in structs for local and server addresses
    struct sockaddr_in remoteaddr, localaddr;
    remoteaddr.sin_addr.s_addr = addr.s_addr;
    remoteaddr.sin_port = port;
    remoteaddr.sin_family = AF_INET;
    localaddr.sin_family = AF_INET;
    localaddr.sin_addr.s_addr = INADDR_ANY;
    socklen_t remoteaddr_len = sizeof(struct sockaddr);

    //Create the socket for the conexion
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0)
    {
        // Print error code and exit.
        perror("socket()");
        exit(EXIT_FAILURE);
    }

    //Bind the socket to local address
    if (bind(sock, (struct sockaddr *)&localaddr, sizeof(localaddr)) < 0)
    {
        // Print error code and exit.
        perror("bind()");
        exit(EXIT_FAILURE);
    }

    //Send a String to the server tohave an answer
    if (sendto(sock, ask, strlen(ask), 0, (struct sockaddr *)&remoteaddr, sizeof(remoteaddr)) < 0)
    {
        // Print error code and exit.
        perror("sendto()");
        exit(EXIT_FAILURE);
    }

    //Revceive the quote from the server
    if (recvfrom(sock, answ, 512, 0, (struct sockaddr *)&remoteaddr, &remoteaddr_len) < 0)
    {
        // Print error code and exit.
        perror("recvfrom()");
        exit(EXIT_FAILURE);
    }

    //Last print the answer
    printf("%s\n", answ);

    //Close the socket used
    if (close(sock) < 0)
    {
        // Print error code and exit.
        perror("close()");
        exit(EXIT_FAILURE);
    }

    return 0;
}