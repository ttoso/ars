// Practica tema 7, Román López Antonio
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <sys/types.h>
#include <unistd.h>
#include <string.h>

int main(int argc, char *argv[])
{
    if (argc > 5)
    {
        fprintf(stderr, "%s Too much arguments\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    //Struct in_addr type to save the IP passed at the parameter.
    struct in_addr addr;
    int err = inet_aton(argv[1], &addr);
    if (err == 0)
    {
        // Print error code and exit.
        perror("inet_atom()");
        exit(EXIT_FAILURE);
    }

    //Asign port number given from the user or use the asigned for the service

    int port;
    struct servent *serv = getservbyname("tftp", "udp");
    port = serv->s_port;

    //Create the request
    char answ[516];
    char mode[] = "octet";
    char ack[4] = "\0\4";
    char rq[516];
    int rqLen;
    FILE *file;
    //Read request
    if (strcmp("-w", argv[2]))
    {
        rqLen = 4 + strlen(argv[3]) + strlen(mode);
        memset(rq, 0, sizeof(rq));
        rq[1] = 1;
        strcpy(rq + 2, argv[3]);
        strcpy(rq + 3 + strlen(argv[3]), mode);
    }
    else //Write request
    {
        rqLen = 4 + strlen(argv[3]) + strlen(mode);
        memset(rq, 0, sizeof(rq));
        rq[1] = 2;
        strcpy(rq + 2, argv[3]);
        strcpy(rq + 3 + strlen(argv[3]), mode);
    }

    //Create sockaddr_in structs for local and server addresses
    struct sockaddr_in remoteaddr, localaddr;
    remoteaddr.sin_addr.s_addr = addr.s_addr;
    remoteaddr.sin_port = port;
    remoteaddr.sin_family = AF_INET;
    localaddr.sin_family = AF_INET;
    localaddr.sin_addr.s_addr = INADDR_ANY;
    socklen_t remoteaddr_len = sizeof(struct sockaddr);

    //Create the socket for the conexion
    int sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0)
    {
        // Print error code and exit.
        perror("socket()");
        exit(EXIT_FAILURE);
    }

    //Bind the socket to local address
    if (bind(sock, (struct sockaddr *)&localaddr, sizeof(localaddr)) < 0)
    {
        // Print error code and exit.
        perror("bind()");
        exit(EXIT_FAILURE);
    }

    if (strcmp("-w", argv[2]))
    {
        file = fopen(argv[3], "w");
        //Send the read  request to the server
        if (sendto(sock, rq, rqLen, 0, (struct sockaddr *)&remoteaddr, sizeof(remoteaddr)) < 0)
        {
            // Print error code and exit.
            perror("sendto()");
            exit(EXIT_FAILURE);
        }

        if (argc > 4 && !strcmp("-v", argv[4]))
        {
            printf("Enviada solicitud de lectura de %s\n", argv[3]);
        }

        //Revceive the file from the server
        int answLen = 516;
        short pcktNumber = 1;
        short total = 0;
        while (answLen == 516)
        {
            answLen = recvfrom(sock, answ, 516, 0, (struct sockaddr *)&remoteaddr, &remoteaddr_len);
            if (answLen < 0)
            {
                // Print error code and exit.
                perror("recvfrom()");
                exit(EXIT_FAILURE);
            }
            if (argc > 4 && !strcmp("-v", argv[4]))
            {
                printf("Recibido bloque del servidor tftp\n");
            }

            //Calculate the package number
            short first = (unsigned char)answ[2] * 256;
            short second = (unsigned char)answ[3];
            total = first + second;
            if (argc > 4 && !strcmp("-v", argv[4]))
            {
                printf("Numero de bloque %d\n", total);
            }
            //Control the possible error package and exit if there is an error
            if (answ[1] == 5)
            {
                printf("Error en el servidor: %s", answ + 3);
            }

            if (total == pcktNumber)
            {
                ack[2] = answ[2];
                ack[3] = answ[3];

                //Send the ack for the package
                if (sendto(sock, ack, 4, 0, (struct sockaddr *)&remoteaddr, sizeof(remoteaddr)) < 0)
                {
                    // Print error code and exit.
                    perror("sendto()");
                    exit(EXIT_FAILURE);
                }

                if (argc > 4 && !strcmp("-v", argv[4]))
                {
                    printf("Enviado el ack del bloque %d\n", total);
                }

                //Write the block in the file
                if (fwrite(answ + 4, 1, answLen - 4, file) < 0)
                {
                    // Print error code and exit.
                    perror("fwrite()");
                    exit(EXIT_FAILURE);
                }
                memset(answ, 0, sizeof(answ));
                pcktNumber++;
            }
        }
        //Close the file
        fclose(file);
        if (argc > 4)
            if (!strcmp("-v", argv[4]))
            {
                printf("El bloque %d era el ultimo: cerramos el fichero\n", total);
            }
    }
    else
    {
        char block[516];
        memset(block, 0, sizeof(block));
        file = fopen(argv[3], "r");
        //Send the read  request to the server
        if (sendto(sock, rq, rqLen, 0, (struct sockaddr *)&remoteaddr, sizeof(remoteaddr)) < 0)
        {
            // Print error code and exit.
            perror("sendto()");
            exit(EXIT_FAILURE);
        }
        if (argc > 4 && !strcmp("-v", argv[4]))
        {
            printf("Enviada solicitud de escritura de %s\n", argv[3]);
        }
        char ack[4];
        //Receive the ack for the request
        if (recvfrom(sock, ack, 4, 0, (struct sockaddr *)&remoteaddr, &remoteaddr_len) < 0)
        {
            // Print error code and exit.
            perror("recvfrom()");
            exit(EXIT_FAILURE);
        }

        if (!strcmp("-v", argv[4]))
        {
            printf("Recibido el ack para escritura\n");
        }
        short nBloque = 1;
        int blockLen = 516;
        while (blockLen == 516)
        {
            //Write the package head
            block[1] = 3;
            block[2] = nBloque / 256;
            block[3] = nBloque % 256;
            //Read file data
            blockLen = fread(block + 4, 1, 512, file) + 4;
            if (blockLen < 0)
            {
                // Print error code and exit.
                perror("fread()");
                exit(EXIT_FAILURE);
            }

            //Send the package to the server
            if (sendto(sock, block, blockLen, 0, (struct sockaddr *)&remoteaddr, sizeof(remoteaddr)) < 0)
            {
                // Print error code and exit.
                perror("sendto()");
                exit(EXIT_FAILURE);
            }
            if (!strcmp("-v", argv[4]))
            {
                printf("Enviado el paquete %d\n", nBloque);
            }

            //Receive the ack fot the block
            if (recvfrom(sock, ack, 4, 0, (struct sockaddr *)&remoteaddr, &remoteaddr_len) < 0)
            {
                // Print error code and exit.
                perror("recvfrom()");
                exit(EXIT_FAILURE);
            }
            if (!strcmp("-v", argv[4]))
            {
                printf("Paquete numero %d entregado correctamente\n", nBloque);
            }

            //If ack correct increment the number of package
            if (ack[2] == block[2] && ack[3] == block[3])
                nBloque++;
        }
        fclose(file);
        if (!strcmp("-v", argv[4]))
        {
            printf("El bloque %d era el ultimo: cerramos el fichero\n", nBloque);
        }
    }

    //Close the socket used
    if (close(sock) < 0)
    {
        // Print error code and exit.
        perror("close()");
        exit(EXIT_FAILURE);
    }

    return 0;
}